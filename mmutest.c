/**
 * @file mmutest.c
 * @brief The testing program for mmu.c.
 *
 * @author Peyton Hanel (phanel16@georgefox.edu)
 */

#include <stdio.h>
#include <stdbool.h>
#include "mmu.h"
#include <string.h>


#define INPUT_SIZE  (255)


void tokenizeInput(char** args, char* input, int numArgs);
void killNewLine(char** args, int numArgs);
bool compareCommands(const char* first, const char* second);
int getNumArgs(char* input);
void instruction_loop(char* pagefile, pagetable_t* tbl);
void read_bytes(uint8_t* buf, char* pagefile, pagetable_t* tbl, vaddr_t vaddr,
    int bytes_to_read);
void vaddr_from_instruction(vaddr_t* new_vaddr, char* instruction);
void write_bytes(char* pagefile, pagetable_t* tbl, vaddr_t vaddr,
    const uint8_t* bytes_to_write, int write_length);
void write_zeros(char* pagefile, pagetable_t* tbl, vaddr_t vaddr,
    int write_length);

/**
 * The main for the tester.
 */
int main()
{
    // allocate memory
    global_physical_memory = mm_mem_init(16); // todo: kill magic numbers
    pagetable_t* tbl = pagetable_alloc();
    char* pagefile = "pagefile.sys";
    mm_vmem_init(pagefile, 256);

    // execute instructions from stdin
    instruction_loop(pagefile, tbl);

    // evict all present virtual pages
    for (int i = 0; i < frame_tracker->mem_size; i++) {
        if (frame_tracker->used_physical_frames[i]) {
            mm_page_evict(pagefile, tbl, frame_tracker->entry_pagenum[i]);
        }
    }
    pagetable_free(tbl);
    free(global_physical_memory);
}

/**
 * Reads in an instruction from standard input each iteration for how to process
 * memory data. Each iteration of the loop counts as one "clock tick" for the
 * ages of each pseudo-physical memory.
 *
 * @param pagefile The file that is used for the virtual memory.
 * @param tbl The pagetable containing pagetable entries
 */
void instruction_loop(char* pagefile, pagetable_t* tbl)
{
    const int SUFFICIENTLY_LARGE_SIZE = 50;
    const int MAX_ARGS = 4;

    vaddr_t vaddr;
    char raw_input[INPUT_SIZE];
    int num_args;
    char* instruction[SUFFICIENTLY_LARGE_SIZE];
    uint8_t* read_data = malloc(sizeof(uint8_t) * PAGE_SIZE); // todo: initialize me?
    uint8_t bytes_to_write[MAX_ARGS];

    // gets instructions from stdin and processes them. each iteration updates
    // ages for each page in memory
    bool running = true;
    while(running)
    {
        // marks each frame as unreferenced here at the beginning of loop
        for (int i = 0; i < frame_tracker->mem_size; i++) {
            pte_old(tbl, frame_tracker->entry_pagenum[i]);
        }
        // gets input and tokenizes it into the instruction[] array
        fgets(raw_input, INPUT_SIZE, stdin);
        num_args = getNumArgs(raw_input);
        tokenizeInput(instruction, raw_input, num_args);

        // reads a single byte of data from a page at a given virtual address
        if (compareCommands(instruction[0], "READ")) {

            // turns the input into a virtual address and reads 1 byte
            vaddr_from_instruction(&vaddr, instruction[1]);
            read_bytes(read_data, pagefile, tbl, vaddr, 1);
        }
        // reads a specified number of bytes from the given virtual address
        else if (compareCommands(instruction[0], "READN")) {

            // turns the input into a virtual address and reads n bytes
            vaddr_from_instruction(&vaddr, instruction[1]);
            read_bytes(read_data, pagefile, tbl, vaddr,
                strtoul(instruction[2], NULL, 2));
        }
        // writes a specified byte value at a specified virtual address
        else if (compareCommands(instruction[0], "WRITE")) {

            vaddr_from_instruction(&vaddr, instruction[1]);

            // turns the instruction string into data and writes it at vaddr
            bytes_to_write[0] = strtoul(instruction[2], NULL, 2);
            write_bytes(pagefile, tbl, vaddr, bytes_to_write, 1);
        }
        // writes 2 bytes of data to a specified virtual address
        else if (compareCommands(instruction[0], "WRITEW")) {

            vaddr_from_instruction(&vaddr, instruction[1]);

            // turns the instruction string into data and writes it at vaddr
            bytes_to_write[0] = strtoul(instruction[2], NULL, 2);
            bytes_to_write[1] = strtoul(instruction[3], NULL, 2);
            write_bytes(pagefile, tbl, vaddr, bytes_to_write, 2);
        }
        // writes 4 bytes of data to a specified virtual address
        else if (compareCommands(instruction[0], "WRITEDW")) {

            vaddr_from_instruction(&vaddr, instruction[1]);

            // turns the instruction string into data and writes it at vaddr
            bytes_to_write[0] = strtoul(instruction[2], NULL, 2);
            bytes_to_write[1] = strtoul(instruction[3], NULL, 2);
            bytes_to_write[2] = strtoul(instruction[4], NULL, 2);
            bytes_to_write[3] = strtoul(instruction[5], NULL, 2);
            write_bytes(pagefile, tbl, vaddr, bytes_to_write, 4);
        }
        // write zeros to pages at a specified virtual address
        else if (compareCommands(instruction[0], "WRITEZ")) {

            vaddr_from_instruction(&vaddr, instruction[1]);
            write_zeros(pagefile, tbl, vaddr,
                strtoul(instruction[2], NULL, 2));
        }
        // exit the loop
        else if (compareCommands(instruction[0], "HALT")) {
            running = false;
        }
        // update ages for each frame based on NFU algorithm (right shifts
        // each age counter by 1, then fills in the leftmost bit with the
        // page's R bit).
        for (int i = 0; i < frame_tracker->mem_size; i++) {

            // only if the frame is being used
            if (frame_tracker->used_physical_frames[i]) {
                frame_tracker->frame_ages[i] >>= 1;
                frame_tracker->frame_ages[i] |=
                    pte_young(tbl, frame_tracker->entry_pagenum[i]) << 31;
            }
        }
    }// while()

    free(read_data);
}

/**
 * Translates the instruction into a virtual address.
 *
 * @param new_vaddr The virtual address to be filled in with data.
 * @param instruction A 20-bit long binary number, the first 8 bits being
 *                    the pagenum and the last 12 being the offset
 */
void vaddr_from_instruction(vaddr_t* new_vaddr, char* instruction)
{
    // creates a virtual address from a string of bits
    unsigned long arg1 = strtoul(instruction, NULL, 2);
    new_vaddr->pagenum = (arg1 & 0xFF000) >> PAGE_SHIFT;
    new_vaddr->offset = (arg1 & 0x00FFF);
}

/**
 * Reads a specified number of bytes starting at a specified virtual address.
 * Sets the referenced bit on each page that was read. Stops reading if the
 * end of the virtual memory is reached. Calculate where to start reading
 * before calling this so that you don't get stupid errors your friends don't
 * call you stupid for what you did.
 *
 * @param buf The array that will have the data read into.
 * @param vaddr The virtual address to specify where to start reading.
 * @param bytes_to_read The number of bytes to read.
 */
void read_bytes(uint8_t* buf, char* pagefile, pagetable_t* tbl, vaddr_t vaddr,
    int bytes_to_read)
{
    // gets the requested page
    page_t* page = pte_page(pagefile, tbl, vaddr.pagenum);
    pte_mkyoung(tbl, vaddr.pagenum);

    // reads a specified number of bytes into buf
    bool at_end = false;
    for (int i = 0; i < bytes_to_read && !at_end; i++) {
        buf[i] = page->data[vaddr.offset];
        vaddr.offset++;

        // if we reach the end of the page, go to the next page
        if (vaddr.offset == 0) { // zero because it overflowed
            vaddr.pagenum++;

            // only reads from the next page if we are not at and of pagetable
            if (vaddr.pagenum == 0) { // zero because it overflowed
                at_end = true;
            }
            else {
                page = pte_page(pagefile, tbl, vaddr.pagenum);
                pte_mkyoung(tbl, vaddr.pagenum);
            }
        }
    }
}

/**
 * Writes specific data to memory beginning at a specified virtual address.
 * Stops writing if the end of virtual memory is reached. Sets the modified
 * and referenced bit for each page that was written.
 *
 * @param pagefile The file on disk used for virtual memory.
 * @param tbl The pagetable with all virtual addresses and page entries.
 * @param vaddr The virtual address to begin writing data at.
 * @param bytes_to_write The array of specific data that will be writen.
 * @param write_length The length of the bytes_to_write array
 */
void write_bytes(char* pagefile, pagetable_t* tbl, vaddr_t vaddr,
    const uint8_t* bytes_to_write, int write_length)
{
    // gets the page and writes data to it
    page_t* page = pte_page(pagefile, tbl, vaddr.pagenum);
    pte_mkdirty(tbl, vaddr.pagenum);
    pte_mkyoung(tbl, vaddr.pagenum);

    // writes the bytes
    bool at_end = false;
    for (int i = 0; i < write_length && !at_end; i++) {
        page->data[vaddr.offset] = bytes_to_write[i];
        vaddr.offset++;

        // if we reach the end of the page, go to the next page
        if (vaddr.offset == 0) { // zero because it overflowed
            vaddr.pagenum++;

            // only writes to next page if we are not at the end of pagetable
            if (vaddr.pagenum == 0) { // zero because it overflowed
                at_end = true;
            }
            else {
                page = pte_page(pagefile, tbl, vaddr.pagenum);
                pte_mkdirty(tbl, vaddr.pagenum);
                pte_mkyoung(tbl, vaddr.pagenum);
            }
        }
    }
}

/**
 * Writes a zero value for a specified number of bytes starting at a specified
 * virtual address. Stops writing if the end of virtual memory is reached. Sets
 * the modified and referenced bit for each page that was written.
 *
 * @param pagefile The file on disk used for virtual memory.
 * @param tbl The pagetable with all virtual addresses and page entries.
 * @param vaddr The virtual address to begin writing zeros at.
 * @param write_length The number of bytes to write as zeros.
 */
void write_zeros(char* pagefile, pagetable_t* tbl, vaddr_t vaddr,
    int write_length)
{
    // gets the page and writes data to it
    page_t* page = pte_page(pagefile, tbl, vaddr.pagenum);
    pte_mkdirty(tbl, vaddr.pagenum);
    pte_mkyoung(tbl, vaddr.pagenum);

    // writes the bytes
    bool at_end = false;
    for (int i = 0; i < write_length && !at_end; i++) {
        page->data[vaddr.offset] = 0;
        vaddr.offset++;

        // if we reach the end of the page, go to the next page
        if (vaddr.offset == 0) { // zero because it overflowed
            vaddr.pagenum++;

            // only writes to next page if we are not at the end of pagetable
            if (vaddr.pagenum == 0) { // zero because it overflowed
                at_end = true;
            }
            else {
                page = pte_page(pagefile, tbl, vaddr.pagenum);
                pte_mkdirty(tbl, vaddr.pagenum);
                pte_mkyoung(tbl, vaddr.pagenum);
            }
        }
    }
}

//TODO: The following methods are from my shell, so share them instead of
// duplicating them

/**
 * Gets the number of tokens separated by spaces from input.
 *
 * @param input Contains the original user input.
 * @return The number of tokens separated by spaces from input.
 */
int getNumArgs(char* input)
{
    int numArgs = 0;
    char previousChar = ' ';
    bool done = false;

    // counts the number of arguments in input. Arguments separated by spaces
    for (int i = 0; i < INPUT_SIZE && !done; i++)
    {
        // reached the end of an argument, increment count
        if (input[i] == ' ' && previousChar != ' ') {
            numArgs++;
        }
        // at end of input, done
        if (input[i] == '\n' || input[i] == 0) {
            numArgs++;
            done = true;
        }
        previousChar = input[i];
    }
    return numArgs;
}

/**
 * Copies each token separated by spaces from input and puts each into args as
 * its own array element.
 *
 * @param input Contains the original user input.
 * @param args An empty char** that will be filled with elements from input. The
 *             first element will be the actual command, the following elements
 *             will be parameters for that command, and the last element will
 *             be NULL.
 * @param numArgs The number of space separated tokens in input. Also the number
 *                of elements that will be in args, excluding the null at the
 *                end.
 */
void tokenizeInput(char** args, char* input, int numArgs)
{
    // todo: process tabs too
    // multiple calls to strtok puts each arg token from input into args
    args[0] = strtok(input, " ");
    for (int i = 1; i <= numArgs; i++) {
        args[i] = strtok(NULL, " ");
    }
    // remove new line from last arg
    killNewLine(args, numArgs);
}

/**
 * Removes a new line character from the end of the last element in args, if
 * there is one.
 *
 * @param args The arguments from user input, the first being an actual command,
 *             the following elements being parameters for that command, and the
 *             last element being NULL.
 * @param numArgs Excluding the NULL at the end, The number of elements in args.
 */
void killNewLine(char** args, int numArgs)
{
    // purges new line char from last arg
    char *newline = strchr(args[numArgs - 1], '\n');
    if (newline) {
        *newline = 0;
    }
}

/**
 * Compares two strings together, both of which are commands. Compares
 * character by character, until an either a new line character or an ascii
 * null is reached.
 *
 * @param first The first command.
 * @param second The second command.
 *
 * @return True if both commands, before the end characters, are the same.
 */
bool compareCommands(const char* first, const char* second)
{
    // compares the two strings, char by char.
    // stops at the new line or null character
    int i = 0;
    bool areEqual = true;
    bool done = false;
    while(!done && areEqual && i < INPUT_SIZE)
    {
        // checks if we are at end of either
        if (first[i]  == '\n' || first[i]  == 0
            || second[i] == '\n' || second[i] == 0)
        {
            // is one of these shorter than the other?
            done = true;
            if (first[i] != second[i]) {
                areEqual = false;
            }
        }
        // are these the same so far?
        if (first[i] != second[i]) {
            areEqual = false;
        }
        i++;
    }
    // stops function from returning true if any of the parameters start with
    // an end character
    if (areEqual
        && (first[0]  == '\n' || first[0]  == 0
            ||  second[0] == '\n' || second[0] == 0))
    {
        areEqual = false;
    }
    return areEqual;
}
