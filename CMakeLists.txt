cmake_minimum_required(VERSION 3.7.2)
PROJECT(mmu)

set(CMAKE_C_STANDARD 11)

add_executable(mmu mmu.c mmutest.c)
