# MMU

Memory management unit simulation program for
[CSIS 460](https://bsnider.cs.georgefox.edu/courses/csis460-operating-systems).

To run, clone to local and run mmutest.c file. This will generate a pagefile.sys
with the bits from the virtual memory processes that you can look at when the
program has terminated. Upon running it, the program will begin to look for input
from standard in. Compiles with gcc.

The following commands are the only things it accepts. All numbers are in binary.

VADDR = { // virtual address struct<br />
  8 bits, // virtual page number<br />
  12 bits // offset within page<br />
}

- READ [VADDR] — read one byte at the specified virtual address.
- READN [VADDR] [NBYTES] — read the specified number of bytes starting at the specified virtual address.
- WRITE [VADDR] [VALUE] — write the specified byte value at the specified virtual address.
- WRITEW [VADDR] [VALUE] [VALUE] — write the specified word (2 byte) value starting at the specified virtual address.
- WRITEDW [VADDR] [VALUE] [VALUE] [VALUE] [VALUE] — write the specified double word (4 byte) value starting at the specified virtual address.
- WRITEZ [VADDR] [NBYTES]— write a zero value for the specified number of bytes starting at the specified virtual address.
- HALT — halt execution; exit the instruction processing loop, evict all present pages to the backing page file, then exit the program.

e.g.: READN 00000011000000000101 00001111

# Change Log

<h2>MMU v1</h2>
<h4>2019-03-16</h4>

**Added**

- MMU API
- Tester framework

**Upcoming Features**

- Actual memory management simulation

# Developer Info

Created by Peyton Hanel
