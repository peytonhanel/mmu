/**
 * @file mmu.c
 * @brief A memory management unit (MMU) simulation.
 *
 * @author Peyton Hanel (phanel16@georgefox.edu)
 */

#include "mmu.h"

pagetable_t* pagetable_alloc()
{
    // allocate a pagetable_t and its entries
    pagetable_t* tbl = malloc(sizeof(pagetable_t));
    if (tbl != NULL) {
        tbl->entries = malloc(PAGETABLE_SIZE * sizeof(pte_t));
        if (tbl->entries != NULL) {
            tbl->size = PAGETABLE_SIZE;
        }
        else {
            free(tbl);
            tbl = NULL;
        }
    }
    return tbl;
}

void pagetable_free(pagetable_t* tbl)
{
    // deallocates the page table
    if (tbl != NULL) {
        free(tbl->entries);
        free(tbl);
    }
}

page_t* mm_mem_init(framenum_t num_frames)
{
    // todo: this is really ugly, can i make it cleaner?
    // it works right now so fix it later.

    // initializes new memory
    global_physical_memory = malloc(num_frames * PAGE_SIZE);

    // initializes the mem_tracker_t that tells us which frames are in use
    if (global_physical_memory != NULL)
    {
        // initializes the mem_tracker_t that tells us which frames are in use
        frame_tracker = malloc(sizeof(mem_tracker_t));
        if (frame_tracker != NULL)
        {
            // initializes used_physical_frames with all elements as false
            frame_tracker->used_physical_frames = malloc(num_frames);
            if (frame_tracker->used_physical_frames != NULL)
            {
                // initializes elements
                for (int i = 0; i < num_frames; i++) {
                    frame_tracker->used_physical_frames[i] = false;
                }
                // initializes the pagenum of each frame array
                frame_tracker->entry_pagenum = malloc(num_frames);
                if (frame_tracker->entry_pagenum != NULL)
                {
                    // initializes frame_ages with values of 0
                    frame_tracker->frame_ages
                        = malloc(num_frames * sizeof(uint32_t));
                    if (frame_tracker->frame_ages != NULL)
                    {
                        for (int i = 0; i < num_frames; i++) {
                            frame_tracker->frame_ages[i] = 0U;
                        }
                        frame_tracker->mem_size = num_frames;
                    }
                    // on failure, free everything
                    else
                    {
                        free(frame_tracker->entry_pagenum);
                        frame_tracker->entry_pagenum = NULL;
                        free(frame_tracker->used_physical_frames);
                        frame_tracker->used_physical_frames = NULL;
                        free(frame_tracker);
                        frame_tracker = NULL;
                        free(global_physical_memory);
                        global_physical_memory = NULL;
                    }
                }
                // on failure, free everything
                else
                {
                    free(frame_tracker->used_physical_frames);
                    frame_tracker->used_physical_frames = NULL;
                    free(frame_tracker);
                    frame_tracker = NULL;
                    free(global_physical_memory);
                    global_physical_memory = NULL;
                }
            }
            // on failure, free everything
            else {
                free(frame_tracker);
                frame_tracker = NULL;
                free(global_physical_memory);
                global_physical_memory = NULL;
            }
        }
        // on failure, free everything
        else {
            free(global_physical_memory);
            global_physical_memory = NULL;
        }
    }
    return global_physical_memory;
}

bool mm_vmem_init(char* pagefile, uint16_t num_pages)
{
    int fseek_success;
    char fputc_success;
    int fclose_success;

    // creates a backing page file on disk
    FILE* file = fopen(pagefile, "w");

    fseek_success = fseek(file, num_pages * sizeof(page_t) - 1, SEEK_SET);
    fputc_success = fputc('\0', file);

    fclose_success = fclose(file);

    // if any of the above functions have an error, return false
    return (fseek_success == 0 && fclose_success == 0);
}

void mm_page_evict(char* pagefile, pagetable_t* tbl, pagenum_t pagenum)
{
    FILE* file;
    pte_t* pte = &tbl->entries[pagenum];

    // moves file pointer to pagenum * sizeof(page_t) location and writes the
    // physical frame starting from there, creating a 1-1 mapping of the pte to
    // the page in the file so that the same index can be used for both
    file = fopen(pagefile, "r+b");
    fseek(file, pagenum * sizeof(page_t), SEEK_SET);
    page_t* page = &global_physical_memory[pte->framenum];
    fwrite(page, sizeof(page_t), 1, file);
    fclose(file);

    // notify the pte that it's page is not in physical memory
    pte_mkclean(tbl, pagenum);
    pte_old(tbl, pagenum);
    pte->present = 0;

    // mark the physical frame as unused and clear it's aging counter
    frame_tracker->used_physical_frames[pte->framenum] = false;
    frame_tracker->frame_ages[pte->framenum] = 0;
}

framenum_t chose_frame()
{
    framenum_t chosen_frame;
    uint32_t least_referenced_size;
    framenum_t least_referenced_index;

    // find an open frame
    framenum_t current_frame = 0;
    while(current_frame < frame_tracker->mem_size
        && frame_tracker->used_physical_frames[current_frame])
    {
        current_frame++;
    }
    // open page found so return this one
    if (current_frame < frame_tracker-> mem_size) {
        chosen_frame = current_frame;
    }
    // if no open frame found, use NFU algorithm to chose a page
    else {
        // find the frame with the age counter that has the lowest value.
        least_referenced_size = frame_tracker->frame_ages[0];
        least_referenced_index = 0;
        for (int i = 1; i < frame_tracker->mem_size; i++) {
            if (frame_tracker->frame_ages[i] < least_referenced_size) {
                least_referenced_size = frame_tracker->frame_ages[i];
                least_referenced_index = i;
            }
        }
        chosen_frame = least_referenced_index;
    }
    return chosen_frame;
}

void mm_page_load(char* pagefile, pagetable_t* tbl, pagenum_t pagenum)
{
    FILE* file;
    pte_t* pte = &tbl->entries[pagenum];
    framenum_t frame;

    // use the frame at the requested page's associated pte->framenum
    if (pte->set) {
        frame = pte->framenum;
    }
    // requested page has no entry, chose a frame and make it a new entry there
    else {
        frame = chose_frame(tbl, pagenum);
        set_pte(tbl, pagenum, mk_pte(frame));
    }
    // evict the chosen frame if it is in use
    if (frame_tracker->used_physical_frames[frame]) {
        mm_page_evict(pagefile, tbl, frame_tracker->entry_pagenum[frame]);
    }
    // load the requested page into the chosen frame
    // goes to the spot in file where the page is and reads it in
    file = fopen(pagefile, "r");
    fseek(file, pagenum * sizeof(page_t), SEEK_SET);
    fread(&global_physical_memory[frame], sizeof(page_t), 1,file);
    fclose(file);

    // notifies the pte_ptr that it's page is now in physical memory
    pte->present = 1;

    // clears R and M bits
    pte_old(tbl, pagenum);
    pte_mkclean(tbl, pagenum);

    // notify frame_tracker and clear it's age counter
    frame_tracker->used_physical_frames[pte->framenum] = true;
    frame_tracker->entry_pagenum[pte->framenum] = pagenum;
    frame_tracker->frame_ages[pte->framenum] = 0;
}

page_t* pte_page(char* pagefile, pagetable_t* tbl, pagenum_t pagenum)
{
    pte_t* pte = &tbl->entries[pagenum];

    // load page into pseudo-physical memory
    if (!pte->present) {
        mm_page_load(pagefile, tbl, pagenum);
    }
    return &global_physical_memory[pte->framenum];
}

pte_t mk_pte(framenum_t framenum)
{
    // creates a new pte_t and returns it
    pte_t result = {
        .framenum = framenum,
        .present = false,
        .set = true,
        .M = false,
        .R = false,
    };
    return result;
}

void set_pte(pagetable_t* tbl, pagenum_t pagenum, pte_t pte)
{
    // sets a pte in the table
    pte.set = true;
    tbl->entries[pagenum] = pte;
}

pte_t pte_clear(pagetable_t* tbl, pagenum_t pagenum)
{
    // for the pte_t at pagenum, sets the set bit to false and returns
    pte_t* result = &tbl->entries[pagenum];
    result->set = false;
    return *result;
}

int pte_none(const pagetable_t* tbl, pagenum_t pagenum)
{
    // returns the inverse of the set bit
    return !tbl->entries[pagenum].set;
}

int pte_present(const pagetable_t* tbl, pagenum_t pagenum)
{
    // returns the present bit
    return tbl->entries[pagenum].present;
}

int pte_dirty(const pagetable_t* tbl, pagenum_t pagenum)
{
    // returns the modified bit
    return tbl->entries[pagenum].M;
}

void pte_mkdirty(pagetable_t* tbl, pagenum_t pagenum)
{
    // sets the modified bit
    tbl->entries[pagenum].M = true;
}

void pte_mkclean(pagetable_t* tbl, pagenum_t pagenum)
{
    // clears the modified bit
    tbl->entries[pagenum].M = false;
}

int pte_young(const pagetable_t* tbl, pagenum_t pagenum)
{
    // returns the reference bit
    return tbl->entries[pagenum].R;
}

void pte_mkyoung(pagetable_t* tbl, pagenum_t pagenum)
{
    // sets the reference bit
    tbl->entries[pagenum].R = true;
}

void pte_old(pagetable_t* tbl, pagenum_t pagenum)
{
    // clears the reference bit
    tbl->entries[pagenum].R = false;
}

pte_t pte_val(pagetable_t* tbl, pagenum_t pagenum)
{
    // returns the actual page table
    return tbl->entries[pagenum];
}

addr_t pagetable_translate(const pagetable_t* tbl, const vaddr_t vaddr)
{
    // translates vaddr to addr
    addr_t result = {
        .framenum = tbl->entries[vaddr.pagenum].framenum,
        .offset = vaddr.offset
    };
    return result;
}
