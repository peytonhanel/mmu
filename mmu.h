/**
 * @file mmu.h
 * @brief Function prototypes and type definitions for the MMU.
 *
 * The MMU translates 20-bit virtual addresses to 16-bit physical addresses,
 * and maps 8-bit virtual page numbers to 4-bit physical page frame numbers.
 * This yields a page table with 2^8 page table entries, one per virtual page,
 * mapping to 2^4 page frames in physical memory. Each page is 2^12 bytes,
 * with each byte within a page addressable via the offset specified in the
 * virtual or physical address.
 *
 * @note MMU functions are loosely based on page table management functions
 * in the Linux kernel, described here:
 * https://www.kernel.org/doc/gorman/html/understand/understand006.html
 *
 * @authors Brian R. Snider (bsnider@georgefox.edu),
 *          Peyton Hanel (phanel16@georgefox.edu)
 */

#ifndef MMU_H
#define MMU_H

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


#define PAGETABLE_SIZE  (1UL << 8)
#define PAGE_SHIFT      12U
#define PAGE_SIZE       (1UL << PAGE_SHIFT)


/**
 * @struct page_t
 * @brief A page of data.
 */
typedef struct {
    uint8_t data[PAGE_SIZE];
} page_t;

typedef page_t frame_t;

/**
 * @struct vaddr_t
 * @brief A 20-bit virtual address type.
 */
typedef struct {
    uint32_t            : 12;   /* unused */
    uint32_t pagenum    : 8;    /**< virtual page number */
    uint32_t offset     : 12;   /**< offset within the page */
} vaddr_t;

/**
 * @struct addr_t
 * @brief A 16-bit physical address type.
 */
typedef struct {
    uint32_t            : 16;   /* unused */
    uint32_t framenum   : 4;    /**< physical page frame number */
    uint32_t offset     : 12;   /**< offset within the frame */
} addr_t;

/**
 * @struct pte_t
 * @brief An 8-bit page table entry type.
 */
typedef struct {
    uint8_t framenum    : 4;    /**< physical page frame number */
    uint8_t present     : 1;    /**< present/absent bit */
    uint8_t set         : 1;    /**< set bit */
    uint8_t M           : 1;    /**< modified bit */
    uint8_t R           : 1;    /**< referenced bit */
} pte_t;

/**
 * @struct pagetable_t
 * @brief A page table type, consisting of many page table entries.
 * @see pagetable_alloc(), pagetable_free().
 */
typedef struct {
    pte_t* entries;             /**< page table entries */
    size_t size;                /**< number of page table entries */
} pagetable_t;

typedef uint8_t pagenum_t;
typedef uint8_t framenum_t;

/**
 * @struct mem_tracker_t
 * @brief Keeps tabs on the pseudo-physical memory. Knows the pagenum for each
 * frame, if that frame is in use, and the size of the pseudo-physical memory.
 */
typedef struct {
    bool* used_physical_frames;
    pagenum_t* entry_pagenum;
    uint32_t* frame_ages;
    unsigned long mem_size;
} mem_tracker_t;


// note: global variable
page_t* global_physical_memory;
mem_tracker_t* frame_tracker;


/**
 * @brief Dynamically allocates a new page table.
 *
 * @return Pointer to a pagetable_t with a size equal to the constant
 * PAGETABLE_SIZE, or NULL if the constant is set to zero.
 */
pagetable_t* pagetable_alloc();

/**
 * @brief Frees the specified page table from memory.
 *
 * Frees the pointers and its associated memory for the pagetable.
 *
 * @param tbl The pagetable to free
 */
void pagetable_free(pagetable_t* tbl);

/**
 * Initializes a new pseudo-physical memory block to the global variable,
 * global_physical_memory. Returns a pointer to this.
 *
 * @param num_frames The size of the memory.
 *
 * @return A pointer to the memory.
 */
page_t* mm_mem_init(framenum_t num_frames);

/**
 * Initializes a page file that acts as virtual memory on disk.
 *
 * @param pagefile The name of this file.
 * @param num_pages The size of this file.
 *
 * @return True if the file was created successfully.
 */
bool mm_vmem_init(char* pagefile, uint16_t num_pages);

/**
 * Removes a page from pseudo-physical memory and writes it to the virtual
 * memory pagefile. Clears its corresponding page table entry's present bit.
 *
 * @param pagefile The file that acts as virtual memory.
 * @param tbl The pagetable containing page table entries.
 * @param pagenum The virtual address of the page that is to be evicted.
 */
void mm_page_evict(char* pagefile, pagetable_t* tbl, pagenum_t pagenum);

/**
 * Searches for the first frame that is currently unused and returns it. If all
 * frames are in use, uses the Not Frequently Used page replacement algorithm
 * to chose a frame.
 *
 * @return Any frame in pseudo-physical memory, not guaranteed to be unused.
 */
framenum_t chose_frame();

/**
 * Copies a page from the virtual memory file pagefile into pseudo-physical
 * memory and sets its corresponding page table entry's present bit.
 *
 * @param pagefile The file that acts as virtual memory.
 * @param tbl The pagetable containing page table entries.
 * @param pagenum The virtual address of the page that is to be loaded into
 *                pseudo-physical memory.
 */
void mm_page_load(char* pagefile, pagetable_t* tbl, pagenum_t pagenum);

/**
 * Returns a pointer to the page mapped to the pagenum. Puts the page into
 * pseudo-physical memory if it is not already there.
 *
 * @param pagefile The file that acts as virtual memory.
 * @param tbl The pagetable containing page table entries.
 * @param pagenum The virtual address of the page to get a pointer to.
 *
 * @return A pointer to the page mapped to pagenum, in pseudo-physical memory.
 */
page_t* pte_page(char* pagefile, pagetable_t* tbl, pagenum_t pagenum);

/**
 * @brief Makes a new page table entry.
 *
 * Creates statically allocated pte_t struct and sets its framenum element
 * to the framenum parameter.
 *
 * @param framenum The value of pte_t.framenum.
 *
 * @return A new pte_t.
 */
pte_t mk_pte(framenum_t framenum);


/**
 * @brief Sets the page table entry for the given virtual page number.
 *
 * @param tbl The pagetable that will have an entry set.
 * @param pagenum The virtual page number of the page to be set in tbl.
 * @param pte The page entry to be put in the tbl.
 */
void set_pte(pagetable_t* tbl, pagenum_t pagenum, pte_t pte);


/**
 * @brief Clears the page table entry for the given virtual page number.
 *
 * Clears the page table entry from the table and returns it.
 *
 * @param tbl The pagetable to clear an entry from
 * @param pagenum The page at this page number will be cleared.
 *
 * @return The page entry that was cleared.
 */
pte_t pte_clear(pagetable_t* tbl, pagenum_t pagenum);


/**
 * @brief Returns 1 if the given page table entry is clear (i.e., not set).
 *
 * @param tbl The pagetable for the page table entry we are referencing.
 * @param pagenum The page table entry to get a value from.
 *
 * @return 1 if the page table entry is not set.
 */
int pte_none(const pagetable_t* tbl, pagenum_t pagenum);


/**
 * @brief Returns 1 if the given page is present.
 *
 * @param tbl The pagetable for the page table entry we are referencing.
 * @param pagenum The page table entry to get a value from.
 *
 * @return 1 if the page is present.
 */
int pte_present(const pagetable_t* tbl, pagenum_t pagenum);


/**
 * @brief Returns 1 if the given page is modified.
 *
 * @param tbl The pagetable for the page table entry we are referencing.
 * @param pagenum The page table entry to get a value from.
 *
 * @return 1 if the page is modified.
 */
int pte_dirty(const pagetable_t* tbl, pagenum_t pagenum);


/**
 * @brief Sets the modified bit for the given page.
 *
 * @param tbl The pagetable for the page table entry we are referencing.
 * @param pagenum The page table entry to modify.
 */
void pte_mkdirty(pagetable_t* tbl, pagenum_t pagenum);


/**
 * @brief Clears the modified bit for the given page.
 *
 * @param tbl The pagetable for the page table entry we are referencing.
 * @param pagenum The page table entry to modify.
 */
void pte_mkclean(pagetable_t* tbl, pagenum_t pagenum);


/**
 * @brief Returns 1 if the given page has been recently referenced.
 *
 * @param tbl The pagetable for the page table entry we are referencing.
 * @param pagenum The page table entry to get a value from.
 */
int pte_young(const pagetable_t* tbl, pagenum_t pagenum);


/**
 * @brief Sets the referenced bit for the given page.
 *
 * @param tbl The pagetable for the page table entry we are referencing.
 * @param pagenum The page table entry to modify.
 */
void pte_mkyoung(pagetable_t* tbl, pagenum_t pagenum);


/**
 * @brief Clears the referenced bit for the given page.
 *
 * @param tbl The pagetable for the page table entry we are referencing.
 * @param pagenum The page table entry to modify.
 */
void pte_old(pagetable_t* tbl, pagenum_t pagenum);


/**
 * @brief Returns the specified page table entry.
 *
 * @param tbl The pagetable for the page table entry we are referencing.
 * @param pagenum The page table entry are getting back.
 *
 * @return The page table entry for the given pagenum.
 */
pte_t pte_val(pagetable_t* tbl, pagenum_t pagenum);


/**
 * @brief Translates a virtual address to a physical address.
 *
 * @param tbl The pagetable for the page table entry we are referencing.
 * @param vaddr The virtual address to be turned into a physical address.
 *
 * @return The physical address that is a translation from a virtual address.
 */
addr_t pagetable_translate(const pagetable_t* tbl, const vaddr_t vaddr);


#endif /* MMU_H */
